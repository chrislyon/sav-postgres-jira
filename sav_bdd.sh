:
## ------------------------------
## SAUVEGARDE BASE POSTGRES
## via pg_dump
## ------------------------------

B_DIR=/sav/sav_bdd
LOG_DIR=${B_DIR}/log
DEV_DIR=${B_DIR}/dev



[ ! -d $LOG_DIR ] && mkdir $LOG_DIR && echo "Erreur DIR $LOG_DIR" >&2

[ ! -d $DEV_DIR ] && mkdir $DEV_DIR && echo "Erreur DIR $DEV_DIR" >&2

TS=$(date "+%A")
DEV_FILE=$DEV_DIR/pg_dump_all_${TS}.gz
LOG_FILE=$LOG_DIR/pg_dump_all_${TS}.log

## ------------------------
## Petite routine de log
## ------------------------
log()
{
        stamp=$(date "+%A %x %X")
        echo ">>> $stamp | $1 "
}

exec 1>$LOG_FILE
exec 2>&1

log "Debut du traitement"

log "Debut de l'export BDD"
TX=$(su - postgres -c "pg_dumpall | gzip -v >$DEV_FILE" 2>&1 )
log "Fin  de l'export BDD"
FIC=$( stat --format="%s" $DEV_FILE )
log "Taux de compression : ${TX}"
log "taille du fichier  : ${FIC} octets"

log "Fin du traitement"
