:
## ------------------------------
## Envoi rapport par mail
## ------------------------------

B_DIR=/sav/sav_bdd
LOG_DIR=${B_DIR}/log

TS=$(date "+%A")
LOG_FILE=$LOG_DIR/pg_dump_all_${TS}.log

TS=$(date "+%A")
MAIL_LOG_FILE=$LOG_DIR/sendmail_${TS}.log

## ------------------------
## Petite routine de log
## ------------------------
log()
{
        stamp=$(date "+%A %x %X")
        echo ">>> $stamp | $1 "
}

exec 1>$MAIL_LOG_FILE
exec 2>&1

JOUR=$(date "+%x %X")
BODY=$(cat $LOG_FILE)
DEST="cbonnet@sra.fr"
HOST="SRAJ01"

log "Envoi du mail"

mail -s "Rapport SAV ${HOST} du ${JOUR}" $DEST <<!!

Rapport du ${JOUR}

-------------- START -----------------
${BODY}
-------------- END -----------------
!!

log "Status envoi : $?"
